***

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.  

Pour toute demande de renseignement, merci de contacter l'équipe [ELAN](http://archives-theatres.elan-numerique.fr/) : litt-et-arts-elan@univ-grenoble-alpes.fr  

***

# Générateur automatique de Header TEI  

## Générer un en-tête TEI à partir des réponses données

***

1. [Comment ça marche ?](#1)
2. [Générer un en-tête TEI à partir du formulaire "Décrire un projet de recherche et ses sources" (ID 557824)](#2)  
    2.1 [Préparation](#2.1)  
    2.2 [Export des réponses au format HTML](#2.2)  
    2.3 [Exécution du script de conversion](#2.3)  
    2.4 [Transformation XSLT](#2.4)  
3. [Générer un en-tête TEI à partir du formulaire "Décrire un fichier numérique, ses sources, les choix d'édition" (ID 292461)](#3)  
    3.1 [Préparation](#3.1)  
    3.2 [Export des réponses au format HTML](#3.2)  
    3.3 [Exécution du script de conversion](#3.3)  
    3.4 [Transformation XSLT](#3.4)  
4. [Personnaliser le en-tête TEI généré](#4)

***

<a name="1"/>

## 1. Comment ça marche ?

Chaque réponse saisie dans le formulaire est associée à l’**identifiant** [ID] de la question correspondante. 

Les réponses données sont exportées au format **HTML**. L’export au format HTML permet de générer un **tableau** où les réponses données sont associées à l’identifiant de la question correspondante.  

Nous souhaitons obtenir un en-tête XML-TEI en partant de ce tableau HTML. Pour ce faire, il faut faire en sorte que le contenu du fichier HTML puisse être transformé par une **feuille** **XSLT**. 

Or, la **transformation XSLT** ne peut pas être appliquée directement sur un fichier HTML. Pour effectuer une transformation XSLT, on utilisera un **script** **Python** pour modifier le **langage d’encodage** du tableau contenant les réponses. 

Le script Python **remplace certaines balises** du fichier HTML par des balises XML génériques, et insère le tableau ainsi encodé dans un nouveau **fichier XML**. L’encodage de ce tableau ne répond à aucun standard d’encodage, mais cela ne représente pas un problème, car ce qui nous intéresse, c’est son contenu : les réponses au questionnaire et leurs identifiants sont structurés d’une manière telle à nous permettre de les extraire au moyen d’une feuille XSLT.

La feuille de **transformation XSLT**, alors, s’applique au fichier XML généré par le script Python. Tout d’abord, elle récupère les réponses données au questionnaire grâce à une **expression XPath** qui appelle leurs identifiants ; ensuite, elle insère chaque reponse dans la **balise correspondante** du header TEI. 

Le résultat de la transformation est un **fichier XML** contenant un **en-tête TEI** où chaque réponse donné au formulaire est insérée dans sa balise correspondante.

### Résumé du procédé

Pour résumer : 

1. Depuis l’interface LimeSurvey, les réponses sont exportées sous forme d’un **fichier HTML**, où les réponses et leurs identifiants sont stockés dans un tableau ; 
2. un **script** **Python** remplace certaines balises HTML du fichier exporté par des balises XML, et insère le tableau encodé en XML dans un nouveau **fichier XML** ;
3. une **feuille de transformation XSLT** s’applique au fichier XML produit par le script Python, récupère chaque réponse grâce à son identifiant,  insère chaque réponse dans la balise correspondante de l’en-tête TEI, et génère un nouveau fichier XML contenant l’en-tête TEI complet.

<a name="1.1"/>

### Rappel : structure des questionnaires

Pour rappel, le générateur d'en-tête TEI est composé de cinq questionnaires.  

* **Générateur d'en-tête TEI** (ID 534613)
* **Générateur d'en-tête TEI : formulaires guidés** (ID 643918)
* **Décrire un projet de recherche et ses sources** (ID 557824)  
* **Décrire un fichier numérique, ses sources, les choix d'édition** (ID 292461) 
* **Générateur d'en-tête TEI : formulaire spécialisé** (ID 162445) [temporairement désactivé]

Parmi ces cinq questionnaires, deux génèrent un en-tête TEI :  

* **Décrire un projet de recherche et ses sources** (ID 557824)  
* **Décrire un fichier numérique, ses sources, les choix d'édition** (ID 292461)  

### Nota bene

<u>**Les instructions ci-dessous peuvent être appliquées uniquement si le participant au questionnaire a soumis ses réponses en cliquant sur le bouton “Envoyer”**</u> ; en effet, l’export des réponses n’est pas possible pour les réponses “Enregistrées mais non envoyées”.

***

<a name="2"/>

### 2. Générer un en-tête TEI à partir du formulaire  "Décrire un projet de recherche et ses sources" (ID 557824)  

<a name="2.1"/>

#### 2.1 Préparation

1. Reportez-vous à la liste des questionnaires en cliquant sur **Questionnaires** en haut de la page.  
2. Cliquez sur le questionnaire **Décrire un projet de recherche et ses sources** (ID 557824).   
3. Reportez-vous au menu latéral de gauche, section **Menu de questionnaire**.  

A partir d'ici, la génération de l'en-tête TEI se fera en trois grandes étapes. Tous les fichiers utilisés dans ce procédé se trouvent dans le dossier **generateur-entete-tei** > **procedure_extraction_entete**.    

<a name="2.2"/>

#### 2.2 Export des réponses au format HTML  

1. Dans la section **Menu de questionnaire**, cliquez sur l'option **Réponses** ;
2. Cliquez sur **Afficher les réponses** ;  
3. Cochez les réponses que vous souhaitez exporter ; 
4. Cliquez sur le bouton **Exporter** situé en haut à droite ;  
5. Dans le menu déroulant, cliquez sur **Exporter les Réponses** ;  
6. Dans le bloc **Format**, cochez **HTML** ;  
7. Dans le bloc **En-tête**, cochez **Code de la question** ;  
8. Cliquez sur le bouton **Exporter** en haut à droite ;  
9. Une nouvelle page HTML s'ouvre. Faites clic droit et cliquez sur **Enregistrer sous** ;  
10. Enregistrez la page sous le nom **a_export_projet.html** dans le dossier **procedure_extraction_entete**.  
    *Enregistrer les fichiers sous ces noms n'est pas obligatoire pour le bon déroulement de la procédure, mais vous permettra de suivre plus facilement les instructions suivantes.*  

<a name="2.3"/>

#### 2.3 Exécution du script de conversion  

**Dans un interpréteur Python** (i.e. Spyder) :

1. ouvrez le fichier **procedure_extraction_entete** > **b_execute.py** ; 
2. personnalisez les chemins des fichiers :  
    * fichier d'entrée : **procedure_extraction_entete** > **a_export_projet.html** ;  
    * fichier de sortie : **procedure_extraction_entete** > **c_convert_projet.xml** ;  
3. exécutez le script.

<a name="2.4"/>

#### 2.4 Transformation XSLT  

**Dans un éditeur XML** (i.e. Oxygen) : 

1. ouvrez le fichier de transformation **procedure_extraction_entete** > **d_transform_projet.xsl** ; 
2. configurez la transformation XSLT : 
     * fichier d'entrée : **procedure_extraction_entete** > **c_convert_projet.xml** ;  
     * fichier de sortie : **procedure_extraction_entete** > le nom de fichier souhaité ;  
5. appliquez la transformation.   

Le ficher de sortie résultant de la transformation XSLT présente un en-tête TEI contenant les réponses données au questionnaire correspondant.

***

<a name="3"/>

### 3. Générer un en-tête TEI à partir du formulaire  "Décrire un fichier numérique, ses sources, les choix d'édition" (ID 292461)

<a name="3.1"/>

#### 3.1 Préparation

1. Reportez-vous à la liste des questionnaires en cliquant sur **Questionnaires** en haut de la page.  
2. Cliquez sur le questionnaire **Décrire un fichier numérique, ses sources, les choix d'édition** (ID 292461).  
3. Reportez-vous au menu latéral de gauche, section **Menu de questionnaire**.  

Comme pour le premier questionnaire,  la génération de l'en-tête TEI se fera en trois grandes étapes.  Tous les fichiers utilisés dans ce procédé se trouvent dans le dossier **generateur-entete-tei** > **procedure_extraction_entete**.  

<a name="3.2"/>

#### 3.2. Export des réponses au format HTML  

1. Dans la section **Menu de questionnaire**, cliquez sur l'option **Réponses** ;
2. Cliquez sur **Afficher les réponses** ;  
3. Cochez les réponses que vous souhaitez exporter ; 
4. Cliquez sur le bouton **Exporter** situé en haut à droite ;  
5. Dans le menu déroulant, cliquez sur **Exporter les Réponses** ;  
6. Dans le bloc **Format**, cochez **HTML** ;  
7. Dans le bloc **En-tête**, cochez **Code de la question** ;  
8. Cliquez sur le bouton **Exporter** en haut à droite ;  
9. Une nouvelle page HTML s'ouvre. Faites clic droit et cliquez sur **Enregistrer sous** ;  
10. Enregistrez la page sous le nom **a_export_fichier.html** dans le dossier **procedure_extraction_entete**.  
    *Enregistrer les fichiers sous ces noms n'est pas obligatoire pour le bon déroulement de la procédure, mais vous permettra de suivre plus facilement les instructions suivantes.*  

<a name="3.3"/>

#### 3.3. Exécution du script de conversion  

**Dans un interpréteur Python** (i.e. Spyder) :

1. ouvrez le fichier **procedure_extraction_entete** > **b_execute.py** ; 
2. personnalisez les chemins des fichiers :  
    * fichier d'entrée : **procedure_extraction_entete** > **a_export_fichier.html** ;  
    * fichier de sortie : **procedure_extraction_entete** > **c_convert_fichier.xml** ;  
3. exécutez le script.

<a name="3.4"/>

#### 3.4 Transformation XSLT  

**Dans un éditeur XML** (i.e. Oxygen) : 

1. ouvrez le fichier de transformation **procedure_extraction_entete** > **d_transform_fichier.xsl** ; 
2. configurez la transformation XSLT : 
     * fichier d'entrée : **procedure_extraction_entete** > **c_convert_fichier.xml** ;  
     * fichier de sortie : **procedure_extraction_entete** > le nom de fichier souhaité ;  
5. appliquez la transformation.   

Le ficher de sortie résultant de la transformation XSLT présente un en-tête TEI contenant les réponses données au questionnaire correspondant.

***

<a name="4"/>

## Personnaliser le en-tête TEI









