#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 23:15:23 2019

@author: Maria Diandra CRISTACHE
"""

import re
from bs4 import BeautifulSoup

### PERSONNALISER LE CHEMIN DU FICHIER D'ENTREE ICI ###
soup = BeautifulSoup(open("/home/diandra/Bureau/perso/generateur-en-tete-tei/generateur-header-tei_dev/procedure_extraction_entete/a_export_fichier.html"), "html.parser")


content = str(soup)

# Dans chaque élément <tr>
# supprime l'attribut @data-quid 
# et rajoute un attribut @id avec une valeur égale au texte du sous-élément <td>
for balise in soup.find_all('tr', {'data-qid' : re.compile(r'([0-9])')}):
    del balise['data-qid']
    balise['id'] = balise.td.text

# Pour chaque élément <td> 
# dont le contenu commence par la chaîne de caractères "Info"
# remplace l'élément <td> par une chaîne de caractères vide 
for link in soup.find_all('td', text=re.compile(r'Info(.)*')):
    link.string = ""
    content = str(soup)  
    content = content.replace("<td></td>\n","")
    
### PERSONNALISER LE CHEMIN DU FICHIER DE SORTIE ICI ###
content = content.replace("<!DOCTYPE html>", '<?xml version="1.0" encoding="UTF-8"?>').replace('html', 'xml').replace('body', 'root')
content = re.sub('<head>(.|\n)*?<\/head>', '', content) 

with open("/home/diandra/Bureau/perso/generateur-en-tete-tei/generateur-header-tei_dev/procedure_extraction_entete/c_convert_fichier.xml", 'w') as f:
    f.write(content)

f.close()
