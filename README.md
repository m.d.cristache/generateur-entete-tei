<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.  

Pour toute demande de renseignement, merci de contacter l'équipe [ELAN](http://archives-theatres.elan-numerique.fr/) : litt-et-arts-elan@univ-grenoble-alpes.fr  

***

# Générateur automatique de Header TEI  

## Présentation  

L'équipe ELAN a conçu et développé un générateur automatique d'en-tête TEI sous forme de questionnaire en ligne.  
Le modèle de questionnaire peut être consulté [ici](http://enquete.elan-numerique.fr/index.php/534613?newtest=Y&lang=fr).

***

## Contenu de ce répertoire

Ce repository Gitlab contient les éléments suivants :  
* un dossier **procedure_configuration_formulaire** : contient les fichiers et le mode d'emploi pour la configuration du questionnaire :
    * un fichier README.md : le mode d'emploi pour la configuration du questionnaire ;  
    * un dossier **procedure_configuration_fichiers** : contient les formulaires à configurer, sous forme de fichiers .lss ;  
    * un dossier **generateur-entete-tei_theme.zip** : contient les fichiers de configuration du thème, sous forme de dossier zippé ; 
    * un dossier **xml** ;  
* un dossier **procedure_extraction_entete** :
    * un fichier README.md : le mode d'emploi pour l'extraction de l'en-tête TEI ; 
    * un dossier **procedure_configuration_fichiers** : contient le fichiers permettant d'obtenir les en-têtes TEI ;
    * un dossier 
* un dossier **Documentation** : contient des documents relatifs au projet (rapports de projet, présentations, etc.)

## Par où commencer 

Pour commencer, téléchargez ou clonez ce repository Gitlab.

Ensuite, rendez-vous au mode d'emploi pour la mise en place du questionnaire, dans le dossier [**procedure_configuration_formulaire**](https://gitlab.com/m.d.cristache/generateur-entete-tei/blob/master/procedure_configuration_formulaire)


