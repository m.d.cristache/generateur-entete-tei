# Projet Générateur d'en-tête TEI #

## Rapport au 04 juillet 2019 ##

***

### Table des matières ###

1. [Rappel](#rappel)

    a. [Contexte et encadrement](#rappel_contexte)
    
    b. [Objectifs du projet](#rappel_objectifs)

    c. [Solutions adoptées](#rappel_solutions)

2. [Travaux réalisés](#travaux)

    a. [Modélisation XML](#travaux_XML)
    
    b. [Formulaire LimeSurvey](#travaux_LS)
    
3. [Problèmes rencontrés](#problemes)

***

1. ### Rappel <a name="rappel"/>

    #### a. Contexte et encadrement <a name="rappel_contexte"/> ####

    Ce projet s'inscrit dans le cadre d'une mission de stage proposée au sein du Laboratoire Arts et Pratique du Texte, de l’Image, de l’Ecran et de la Scène (Litt&Arts, UMR 5316, UGA/CNRS). 

    La mission de stage s'effectue au sein de l'équipe ELAN (Equipe Littérature et Arts Numériques) et est encadrée par Anne GARCIA-FERNANDEZ (ingénieure de recherche CNRS) et Elisabeth GRESLOU (ingénieure de recherche UGA).

    Le stage est accueilli dans les bureaux de l'équipe ELAN (MSH, bureau 23 ; bâtiments Stendhal, bureau B3...) et se déroule dans les mois de juin et juillet 2019.

    #### b. Objectifs du projet <a name="rappel_objectifs"/> ####

    Mise en place d'un dispositif de référence pour la standardisation en TEI des métadonnées de projets de recherche (disciplines humanistes) ; accompagnement des chercheuses et chercheurs dans la construction de l'en-tête TEI de leurs corpus.

    #### c. Solutions adoptées <a name="rappel_solutions"/> ####

    Une plateforme en ligne et en libre accès, où les métadonnées sont saisies par les chercheurs dans des champs spécifiques et automatiquement insérées dans les balises correspondantes de l'header TEI.

    Pourquoi une plateforme en ligne ?

    * **familiarité de l'outil** : un type d'outil très courant dans la vie quotidienne et professionnelle (formulaire en ligne : administrations, enquêtes, inscriptions, etc.) => autonomie d'usage ;

    * **potentiel pédagogique** : un support concret favorisant la compréhension et l'appropriation de la logique du langage XML (notions de balisage, arborescence, métadonnées, etc.) pour les chercheuses et chercheurs non familiers avec ce domaine;

    * **accessibilité des notions techniques** : médiation entre le langage scientifique de la recherche et le langage technique de la documentation TEI (stucture massive, organisation complexe, langage concis, notions techniques données pour acquis, éléments ambigus, etc.);

    * **rayon d'utilisation potentiellement vaste** : un outil adapté à tout type de projet basé sur un corpus de données textuelles encodées en XML ;

    * **une viabilité sur le long terme** : une solution techniquement peu complexe, des technologies répandues et éprouvées : maintenance et implémentation technique simples et peu coûteuses
    
    Première proposition : développement d'un formulaire en PHP/MySQL avec stockage des réponses dans une BDD et génération automatique de balisage XML.

    La proposition du formulaire est retenue, mais une solution technique différente est proposée : création d'un formulaire LimeSurvey avec stockage en BDD possible mais non nécessaire. Avantages de cette solution : fonctionnalités complexes mais sans complexité technique (mise en place plus rapide ; transmission du projet à d'autres laboratoires plus simple)



2. ### Travaux réalisés <a name="travaux"/>

    #### a. Modélisation XML <a name="travaux_XML"/> ####

    Avant de procéder à l'élaboration d'un formulaire, il faut se demander quelles informations seront demandées par celui-ci. Le header TEI contient des centaines d'éléments et d'attributs possibles, dont certains sont obligatoires, d'autres recommandés et d'autres encore optionnels. Certains éléments concernent les aspects scientifiques d'un projet (chercheurs impliqués, sources matérielles, etc.), d'autres concernent des aspects purement techniques (schémas d'encodage, langages de développement, etc.). 

    La ampleur du corpus de métadonnées que  peut accueillir un en-tête TEI mène à se poser la question suivante : *faut-il que notre formulaire contienne l'intégralité de ces données ?* Pour répondre à cette question, j'ai d'abord essayé de mieux comprendre la structure des données contenues dans un en-tête TEI. La [documentation TEI](https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/ST.html) propose des explications détaillées pour chaque composante du header TEI, avec des extraits isolés en guise d'exemple. Or, ce que je cherchais, était plutôt une représentation complète de la structure du header, une véritable structure XML contenant tous les éléments susceptibles d'être contenus dans un header TEI. J'ai alors cherché en ligne un modèle XML d'en-tête TEI qui soit le plus complet possible, mais cette recherche n'a pas donné de résultats ; en effet, toute documentation en ligne propose soit le modèle basique déjà proposée par la documentation officielle, soit des extraits plus ou moins personnalisés à titre d'exemple. Deux questions se sont alors posées : *pourquoi n'existe-il pas un modèle complet du header TEI ?* et *est-ce possible de produire ce modèle ?* 

    La réponse à la première question relève, à mon avis, de la conception scientifique à l'origine de l'entête TEI : ce dernier est conçu pour s'adapter aux spécifications du plus grand nombre de projets possible ; c'est un outil qui peut prendre tant de formes différentes qu'il existe de projets de recherche. Dans ce sens, le fait de présenter une structure de données longue et complexe, dont tous les éléments ne sont pas forcément nécessaires, pertinents ou compréhensibles, peut être perçu comme une contrainte et alourdir la tâche de saisie des métadonnées, voir même en dissuader. Loin de là, le point de départ que propose la documentation TEI consiste effectivement en une structure basique, composée d'un petit nombre d'éléments requis et pourvue, grâce aux fonctionnalités des éditeurs XML et à un schéma de données sous-jacent, de facilitations pour l'ajout d'éléments supplémentaires (par exemple, la suggestion automatique des éléments attendus à l'ouverture d'une balise). 

    De fait, ma recherche semble entrer en conflit avec la conception-même du header TEI : d'une part, parce que je ne l'emploie pas pour sa finalité principale (saisie de métadonnées de projet) ; de l'autre, parce que j'en recherche une version complète de tous ses éléments, alors qu'il est conçu pour être construit "brique par brique", selon les spécificités de chaque projet. 

    Ce constat me mène à la deuxième question : est-ce possible de produire une version complète du header TEI ? Pour répondre à cette question, j'ai d'abord cherché des solutions permettant de le faire automatiquement. J'ai alors fait recours à la plateforme [ROMA](https://roma2.tei-c.org/) qui permet de générer des schémas XML personnalisés sur la base du standard TEI. Dans ROMA, l'option "Reduce" permet d'exclure des éléments et/ou attributs depuis "*the largest possible schema*" - un schéma contenant l'intégralités des éléments et attributs TEI. Mon objectif étant justement d'obtenir le schéma "le plus large possible", j'ai choisi cette option, laissé en place l'intégralité des modules intégrés par défaut au schéma et généré le schéma XML tel quel. Cependant, le schéma obtenu affiche uniquement les éléments requis ; il s'agit, de fait, du même modèle basique fourni par la documentation TEI. Ainsi, puisque la tentative de génération automatique n'a pas donné le résultat espéré, je me suis tournée vers la solution alternative : la production manuelle.

    Pour produire manuellement une version complète du header TEI, j'ai créé un schéma XSD basé sur les spécifications fournie par sa [documentation officielle](https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html). Pour recréer de manière exacte la structure des éléments du header, j'ai consulté la documentation sous deux formats différents : d'une part, le format imprimé, pour suivre le fil de la lecture selon une progression "horizontale"(lecture linéaire page par page) ; de l'autre, le format numérique, pour approfondir les spécifications de forme et de contenu de chaque composante du header (liens hypertexte entre de différentes pages).
     Le schéma XSD ainsi élaboré génère un fichier XML contenant un modèle "élargi" du header, où la plus grande partie de ses éléments et attributs (cf [Problèmes rencontrés](#problemes)) sont présents et visibles. 

    La question de la visibilité des composantes du header est centrale pour plusieurs raisons. Tout d'abord, un modèle de header "mis à plat" dans toutes ses composantes donne une idée de l'extension que pourrait atteindre un corpus de métadonnées TEI exploité au maximum. Le modèle basique de header compte environ 30 lignes, alors que le modèle généré par notre schéma en compte 407 (en sachant qu'il contient *une grande partie* des éléments possibles, non pas leur totalité). Ce constat nous ramène à notre question de départ : faut-il que le formulaire contienne l'intégralité des données d'un header TEI ? Virtuellement, c'est une option plausible : tous les champs de métadonnées présents dans le header sont susceptibles d'être remplis dans le cadre de tel ou tel projet. Mais concrètement, il s'agirait de créer un formulaire contenant des centaines et des centaines de questions, et les implications pourraient être lourdes : non seulement de très longs délais de mise en place (formulation de chaque question, génération du champs correspondant dans le formulaire, mise en forme de la réponse, etc.), mais aussi des risques liés à la réception de l'outil de la part du public. En effet, puisque l'on travaille sur une produit web, il faudra prendre en considération ces critères de conception qui se répercutent immédiatement sur l'expérience utilisateur : ergonomie, attractivité, lisibilité... Pour que notre formulaire soit attrayant aux yeux de l'utilisateur, en somme, ce sera peut-être plus indiqué d'"écrémer" la masse de données du header TEI et d'en retenir celles qui sont du plus grand intérêt pour notre public cible (ce qui n'exclue pas la possibilité de rajouter de nouveaux champs par la suite, par exemple sur demandes des utilisateurs eux-mêmes).

    La question de la visibilité du header présente un autre intérêt, cette fois plutôt pratique et lié à la mise en place du formulaire. L'objectif du formulaire est de "construire" un modèle de header TEI autour des réponses apportées par les utilisateurs, ce qui se traduit concrètement par le fait d'insérer chaque réponse de l'utilisateur dans l'élément correspondant du header TEI.  Avec un modèle de header "mis à plat" on aura la possibilité d'extraire des sections plus ou moins étendues de ce modèle, pour les intégrer directement à la solution technique choisie pour le formulaire. De plus, cette démarche permet d' "autonomiser" les tâches au sein de l'équipe, garantissant ainsi une cohérence scientifique globale : avec un schéma XML élaboré au préalable et de façon unitaire, la tâche de mise en place du formulaire pourra être prise en charge par plusieurs personnes, ayant cependant une base de travail commune.

    Le développement de ce schéma a fait surgir un certain nombre d'interrogatifs d'ordre technique et méthodologique, qui seront exposées dans la section [Problèmes rencontrés](#problemes).
    
    Le schéma XSD et le fichier XML se trouvent dans le dossier GitLab xml-tei-metadata-formatter > xml > structure-TEI-schema.xsd, structure 1.xml.     

    #### b. Formulaire LimeSurvey <a name="travaux_LS"/> ####


3. ### Problèmes rencontrés <a name="problemes"/>

    #### Modélisation XML ####

    * le schéma élaboré génère un modèle de header contenant la plupart de ses éléments et attributs, mais non pas leur totalité : est-ce *utile* de générer la totalité des éléments du header ? Facteurs à prendre en compte : les temps de mise en place contre l'utilité effective pour les utilisateurs

    #### Formulaire LimeSurvey ####

    * on n'arrive toujours pas à afficher correctement les **chevrons** pour les balises...
    * la **structure du formulaire** LimeSurvey est quelque peu "rigide" par rapport à la structure des données du header. Le header est composé de plusieurs niveaux d'analyse : plusieurs imeSurvey permet également de créer des groupes de questions, mais ces groupes ne peuvent pas contenir de sous-ggroupes d'éléments contiennent plusieurs sous-groupes qui à leur tour contiennent d'autres sous-groupes... Lroupes : ils peuvent contenir uniquement des listes de questions qui sont toutes au même niveau. Cela pose de problèmes notamment par rapport à l'affichage du formulaire, car il n'y a aucune distinction visuelle entre les différentes catégories de questions.
    * quant aux **contenus du formulaire** (explications du contenu attendu pour chaque champs), peut-on utiliser tels quels des textes extraits de la documentation TEI ?
    





