# Projet Générateur d'en-tête TEI #

## Rapport progressif ##

***

<u>**06 juin 14h**</u>

<u>Réunion n°1</u>

* Présentation de la proposition de projet

* Discussion, échange, préparation du CDC

* Observations relatives à la proposition du projet

* Première esquisse d’architecture logicielle

<p>
<img src="images_rapports/tableau-2019.06.06.jpg" width="600"/>
<br>
<em>Première esquisse d’architecture logicielle (main de AnneGF)</em>
</p>

***

<u>**07 juin**</u>

Prise en main de l’OS Ubuntu (opérations en ligne de commande).

***

<u>**09 juin**</u>

Prise en main de XAMPP (installation, navigation, gestion de fichiers PHP) via le terminal Ubuntu

(Re)prise en main du langage PHP (révision et approfondissement des connaissances déjà acquises)

Création d’un repository GitLab “XML-TEI Metadata Formatter” (non partagé à ce jour - en attente de perfectionnement)

Problème :  installation locale de GitLab non réussie (procédé d’installation : https://about.gitlab.com/install/#ubuntu)

<p>
<img src="images_rapports/erreur_gitlab_1.png" width="600"/>
<br>
<em>Configuration ordinateur (Ubuntu 18.10)</em>
</p>

<br>

<p>
<img src="images_rapports/erreur_gitlab_2.png" width="600"/>
<br>
<em>Erreur installation GitLab</em>
</p>

Une [publication](https://gitlab.com/gitlab-org/gitlab-runner/issues/3806) dans le forum de gitlab-runner  indique qu’à ce jour il n’existe pas de paquets d’installation GitLab pour Ubuntu 18.10. La solution alternative proposée en réponse, cependant, n’a pas fonctionné non plus. Est-ce que ça posera un problème pour la gestion du développement ?

mise à jour **12 juin** : Anne propose en alternative d’installer Git et de synchroniser avec le repertoire GitLab

***

<u>**11 juin**</u>

Création d’un espace [Slack](https://join.slack.com/t/stage-elan/shared_invite/enQtNjYxMjczNDc3OTczLTFmOWE3ODQwY2NmNDgyMGVhYmU0NjkwNjRlYTU1YWM4YmFjNWRjMGFhNWIyZmJjMDUzZDA3MDAwNTEzZGU3MDQ ) relié au dossier Dropbox (envisager la liaison avec le dossier GitLab).

Début d’analysé de la documentation du  teiHeader (types d’éléments, types de contenus, etc.)

Début d’élaboration d’un schéma XML  détaillé (structuration des données, typage, contraintes)

***

<u>**12 juin**</u>

Installation de Git en ligne de commande et synchronisation avec le repertoire GitLab, réussie.

Intégration de GitLab Slack et de GitLab Notification Service

Récapitulatif des instruments de travail collaboratif mis en place :

<p>
<img src="images_rapports/outils_collaboratifs.png" width="900"/>
<br>
<em>Outils de travail collaboratif</em>
</p>

***

<u>**14 juin**</u>

Suite du travail sur le schéma XML

***

<u>**17 juin**</u>

Génération d’un teiHeader structuré via la plateforme Roma. Cette structure présente les éléments principaux de l’Header, mais seuls les éléments obligatoires sont visibles.

<p>
<img src="images_rapports/schema_roma.png" width="600"/>
<br>
<em>En-tête TEI généré automatiquement par la plateforme ROMA</em>
</p>

Tâche : implémenter le schéma de cette structure pour produire l’intégralité des éléments du teiHeader et définir leurs spécifications respectives (typages et contraintes). 

Objectif : répertorier et structurer l’intégralité des informations et des contraintes formelles qui détermineront la forme et le fond du formulaire.

***

<u>**18 juin**</u>

10h45-18h : Journée d’études Fonte Gaia

Suite du travail sur le schéma XML 

***

<u>**19 juin**</u>

11h : réunion de projet HOMERE (Anne, Elisabeth, Christiane Louette). Présente en tant qu’observatrice. Observation focalisée sur les dynamiques de communication et d’échange autour de la mise en place du projet (cf notes papier).

14h30 : réunion équipe ELAN (Anne, Elisabeth, Célia, Arnaud). Présente en tant que collaboratrice-stagiaire et observatrice. Observation du déroulement d’une réunion d’équipe (définition d’un budget prévisionnel, point sur les projets en cours, rappel des deadlines pour les projets, prévision de conférences ou événements, …)

15h30 : réunion de projet PLANETAS (Anne, Elisabeth, Florian Barrière). Présente en tant qu’observatrice. Observation focalisée sur les dynamiques de communication et d’échange autour de la mise en place du projet (cf notes papier).

Suite du travail sur le schéma XML

***

<u>**20 juin**</u>

10h : AG LITT&ARTS

15h30 : réunion de projet (Anne, Elisabeth, Alice Folco)

Travail sur le schéma XML : conclusion de la première tâche (structuration des données). Tâche suivante : définition des typages.

***

<u>**21 juin**</u>

Travail sur le schéma XML.  Rectification : typages et contraintes ne sont pas définis dans le schéma. L’objectif est d’obtenir un schéma d’éléments structuré, qui sera généré pour être intégré aux réponses rentrée dans le formulaire sous forme de texte simple (sans interprétation des balises de la part du navigateur).  De plus, les typages et contraintes peuvent varier de projet en projet, et pourront être définis ultérieurement, par un traitement en XML du header produit par le formulaire.Seules les contraintes liées à la structure même de l’header  (éléments requis ou facultatifs, ordre imposé, etc.) sont spécifiées dans le schéma.
Tâche suivante : approfondissement de la structure des éléments. Le schéma élaboré jusqu’ici contient les éléments et attributs spécifiés par la documentation TEI au chapitre 2 (L’En-tête TEI). A présent, nous pouvons implémenter le contenu de chaque éléments en faisant référence aux autres chapitres de la documentation.
N.b. : les listes composées d’éléments à choix multiple ne sont pas structurés avec la balise &lt;xs:choice&gt;, parce que les éléments contenus dans cette balise sont générés sous forme de commentaires ; pour que tous les éléments apparaissent, elles sont structurées avec la balise &lt;xs:sequence&gt;. C’est une imprécision scientifique mais qui permet de faciliter le passage de l’XML à LimeSurvey : lorsqu’on transfère dans les cases réponse de LS les portions de header correspondantes, c’est plus rapide (et fiable) de supprimer les éléments inutilisés que de recherche et rajouter des éléments manquants.

***

<u>**25 juin**</u>

14h : réunion équipe ELAN (Anne, Elisabeth, Célia, Sylvain). Exposition des travaux effectués et discussion de spécifications techniques (structure et contenus de la plateforme).
Prise en main de la plateforme LimeSurvey avec Anne (*Quelles fonctionnalités adaptées à la structure de données du teiHEader ?*).

***

<u>**26 juin**</u>

Premier maquettage de la plateforme ([maquette](https://drive.google.com/file/d/1t60kBEaJlZBZmHT7WbKoszB-k91h4riu/view?usp=sharing)). 

Rédaction des premières questions, avec élaboration d’un plan d’expression.

***

<u>**04 juillet**</u>

<u>Réunion n°2</u>

* Présentation de l'état des travaux ([compte rendu](rapport_2019.07.04.md))
* Repérage de points faibles => propositions d'amélioration :
    * langage de la plateforme relevant d'un registre excessivement technique => simplification du registre linguistique du formulaire, avec reformulation des notions techniques sous un lexique scientifique plus familière pour l'utilisateur cible ; 
    * organisation des questions calquée sur la logique technique (complexe) de la documentation TEI => réorganisation des questions du formulaire en des catégories d'informations plus proches de la logique scientifique d'un chercheur humaniste ;
    * structure de la plateforme visuellement trop complexe (perte des repères entre les différents groupes et sous-groupes de questions) => adoption de typologies de réponses permettant de réduire au minimum le nombre de clics (*less is more*) et conférant ainsi au formulaire une identité graphique sobre et ergonomique.

<p>
<img src="images_rapports/tableau_2019.07.04.jpg" width="600"/>
<br>
<em>Propositions pour la refonte de la structure du formulaire (main de AnneGF et Diandra)</em>
</p>

*Une remise en question concernant mon positionnement scientifique vis-à-vis des mes ressources : souhaitable un affranchissement du cadrage technique institué par la structure des données du header TEI*


Il est décidé que le premier prototype du formulaire sera maintenu, pour être éventuellement repris par la suite. Cette version, plutôt adaptée à un public déjà familier aux pratiques de modélisation numérique, pourra être implémentée en tant que "version avancée" du formulaire, destinée aux professionnel.le.s du numérique (ingénieur.e.s d'étude et de recherche).<br>
La simplification de ce premier prototype, en revanche, donnera lieu à une version que l'on qualifiera de "basique" et sera conçue pour un public adepte de méthodologies scientifiques plus "traditionnelles" (chercheuses et chercheurs dans les domaines des Humanités).

Pour faire coexister les deux versions, j'ai mis en place trois formulaires LimeSurvey communiquants : 

[1] [Générateur d'en-tête TEI - start](http://enquete.elan-numerique.fr/index.php/534613?lang=fr) : une page d'accueil contenant un lien vers la version basique (formulaire [2]) et un lien vers la version avancée (formulaire [3]) ;

[2] [Générateur d'en-tête TEI : version basique](http://enquete.elan-numerique.fr/index.php/557824?lang=fr) : la version simplifiée du formulaire, en cours de développement ; 

[3] [Générateur d'en-tête TEI : version avancée](http://enquete.elan-numerique.fr/index.php/162445?lang=fr) : la version complexe du formulaire, partiellement développée sous forme de prototype.

<p>
<img src="images_rapports/LS_interface_2019.07.07.png" width="600"/>
<br>
<em>Interface de la page d'accueil (formulaire [1])</em>
</p>

Le formulaire est ainsi structuré pour que toutes les composantes du projet soient rassemblées et accessibles. **La suite immédiate du travail sera focalisée sur la catégorisation scientifique et la structuration des données au sein du formulaire ; les aspects visuels (identité graphique) et expressifs (formulation des contenus) seront définis à un stade ultérieur.**
***

<u>**05 juillet**</u>

Objectif : une meilleure adaptation du formulaire à son utilisateur cible. Élaboration d'une nouvelle méthode de catégorisation des données, fondée sur un lexique scientifique propre à la méthodologie de recherche dans le domaine des Humanités. 

<p>
<img src="images_rapports/tableau_2019.07.05.jpg" width="600"/>
<br>
<em>Propositions de catégorisation des données (main de Diandra)</em>
</p>

***

<u>**08 juillet**</u>

Refonte de la méthode de gestion du projet : 

* suppression de l'espace Slack (inutilisé) ;
* suppression de l'espace Dropbox (inutilisé et superflu : même fonction de stockage que le repository GitLab) ;
* transposition du fichier de suivi de stage (originairement sous Dropbox Paper) au format markdown et dépôt dans le repository Gitlab ('Rapports de projet' &gt; 'rapport_progressif.md') ;
* rassemblement de la documentation de projet dans le repository GitLab :
    * répertoire 'Rapports de projet' : rapports de projet (progressifs et ponctuels) ;
    * répertoire 'Documentation' : toute documentation autre que les rapports de projet ;

***

<u>**12 juillet**</u>

L'élaboration de ces questions tient compte de deux critères formels : leur organisation structurelle (structure) et leur expression stylistique (style).

L'organisation structurelle du formulaire est déterminée par la **catégorisation scientifique** des données requises, chaque donnée étant représentée par une question et chaque question état associée à une catégorie thématique. Les questions du formulaire seront ainsi réparties en des **axes thématiques** élaborés sur la base des **conceptions scientifiques** propres à la **recherche** du domaine **humaniste**.

Les questions du formulaire sont caractérisées à leur tour par des **choix stylistiques**, concernant notamment les tournures de phrase et le registre employé. Puisque le questionnaire est destiné à un public non familier avec le langage XML, on cherchera à formuler des questions employant des **tournures dialogiques** et un **registre non-technique**.

**22 juillet**

Le formulaire est réparti en deux sous-formulaires complémentaires : 

* un macro-header : informations générales sur le projet de recherche, l'équipe de travail, etc.
* un micro-header : informations spécifiques sur chaque fichier XML produit dans le cadre du projet

- titre du fichier
- porteur du projet
- historique = contributeur + contribution
- source

**26 juillet**

La structure du questionnaire est élargie. Le renvoi à un groupe de questions isolé ne fonctionne pas. Mais le problème peut être contourné : au lieu de renvoyer à plusieurs groupes de questions dans le même questionnaire, on peut créer plusieurs petit questionnaires liés entre eux.
Les questionnaires sont ainsi organisés en arborescence : 

<p>
<img src="images_rapports/LS_structure.png" width="600"/>
<br>
<em>Arborescence formulaires</em>
</p>


***
***

### Ressources ###

#### <u>Outils et logiciels</u> ####

* [Oxygen](https://www.oxygenxml.com/) : modélisation XML/XSD ;
* [LimeSurvey](https://www.limesurvey.org/) : mise en place du formulaire ;
* [GitLab](https://gitlab.com/) : stockage et partage des fichiers du projet ;
* [ReText](https://framalibre.org/content/retext) : rédaction des rapports de projet au format markdown

#### <u>Documentation technique</u> ####

##### XML #####
* [Schéma XML](https://openclassrooms.com/fr/courses/1766341-structurez-vos-donnees-avec-xml)
* [Documentation TEI](https://tei-c.org/guidelines/p5//)
* [Plateforme ROMA](https://roma2.tei-c.org/)

##### Formulaire #####

* [LimeSurvey](https://manual.limesurvey.org/)

##### Divers #####
* [Tutoriel Ubuntu](https://doc.ubuntu-fr.org/debutant)
* [Tutoriel XAMPP et PHP](https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql)
* [Documentation GIT]( https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#send-changes-to-gitlabcom)



#### <u>Supports à la rédaction</u> ####

* [CNRTL Ortolang](https://www.cnrtl.fr/portail/)
* [Reverso Context](https://context.reverso.net/traduction/)
* [Synonymo](http://www.synonymo.fr/)