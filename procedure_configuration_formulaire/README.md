***

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.  

Pour toute demande de renseignement, merci de contacter l'équipe [ELAN](http://archives-theatres.elan-numerique.fr/) : litt-et-arts-elan@univ-grenoble-alpes.fr  

***

# Générateur automatique de Header TEI  

## Configuration et personnalisation

***

1. [Etapes préparatoires](#1)

2. [Import du questionnaire](#2) 

3. [Import du thème](#3)

4. [Activation du questionnaire](#4)

5. [Génération de l'en-tête TEI](#5)

6. [Fonctionnalités diverses](#6)

***

<a name="1"/>  

## 1. Étapes préparatoires

1. Assurez-vous d'avoir bien téléchargé ou cloné ce repository Gitlab.                                                                                                                              
2. Accédez à votre espace administrateur dans la plateforme LimeSurvey : [https://www.limesurvey.org/](https://www.limesurvey.org/) 

***

<a name="2"/>  

## 2. Import du questionnaire

### Structure du générateur de en-tête TEI

Le générateur d'en-tête TEI consiste en un groupe de cinq questionnaires reliés entre eux.  

* **Générateur d'en-tête TEI** (ID 534613)
* **Générateur d'en-tête TEI : formulaires guidés** (ID 643918)
* **Décrire un projet de recherche et ses sources** (ID 557824)  
* **Décrire un fichier numérique, ses sources, les choix d'édition** (ID 292461) 
* **Générateur d'en-tête TEI : formulaire spécialisé** (ID 162445). *Ce questionnaire est temporairement désactivé et sera mis à disposition dès que possible.*

### Import du groupe de questionnaires

Notre objectif est d'importer les premiers quatre questionnaires qui composent notre générateur d'en-tête TEI. Ces questionnaires se trouvent dans le dossier **generateur-entete-tei** > **procedure_configuration_formulaire** > **procedure_configuration_fichiers**.  

Nous allons d'abord importer le premier questionnaire.  

#### Importer un questionnaire : explication pas à pas

1. Reportez-vous à la barre de navigation en haut de page. Cliquez sur **Questionnaires**. 
2. Dans la section **Liste des questionnaires**, cliquez sur **Créer un nouveau questionnaire**.  
3. Cliquez sur **Importer**.  
4. Cliquez sur **Browse**.  
5. Ouvrez le dossier **procedure_configuration_formulaire**.  
6. Ouvrez le dossier **procedure_configuration_fichiers**.  
7. Cliquez sur le fichier **limesurvey_survey_534613.lss**. *Remarquez : le chiffre dans le nom de chaque fichier correspod à l'ID du formulaire associé (cf. section "Structure du générateur d'en-tête TEI")* 
8. Le fichier **limesurvey_survey_534613.lss** a été chargé. Dans l'option **Convertir les liens de ressources et les champs d'expression** : sélectionnez **OUI**.  
9. Cliquez sur **Importer un questionnaire**.  

En bas de la page, vous pourrez éventuellement lire deux messages : 
* *“Attention : Il y a quelques différences entre les options actuelles du thème et celles du thème original”*. Cette différence consiste dans le logo appliqué au thème (cf section [Options de thème](#theme_opt)). Si vous cliquez sur **Appliquer et se rendre au thème**, le logo de l'équipe ELAN sera appliqué à votre questionnaire. Pour appliquer votre propre logo, ignorez ce message et reportez-vous à la section [Personnalisation du logo](#4).  
* *“Le thème du questionnaire original n'existe pas”*. Ce message peut apparaître parce que le thème associé à ce questionnaire n'a pas encore été importé. L'importation du thème sera faite à l'étape suivante.

Reportez-vous à la barre de navigation en haut de page. Cliquez sur **Questionnaires**.  

Le formulaire **Générateur d’en-tête TEI**  (ID 534613) apparaît maintenant dans notre liste de questionnaires.  

Répétez le même procédé pour importer les trois autres fichiers contenus dans le dossier **procedure_configuration_fichiers** :  

* le fichier **limesurvey_survey_643918.lss**, correspondant au questionnaire **Générateur d'en-tête TEI : formulaires guidés** (ID 643918) ;  
* le fichier **limesurvey_survey_557824.lss**, correspondant au questionnaire **Décrire un projet de recherche et ses sources** (ID 557824) ;  
* le fichier **limesurvey_survey_292461.lss**, correspondant au questionnaire **Décrire un fichier numérique, ses sources, les choix d'édition** (ID 292461) ;  

Le cinquième questionnaire du groupe, **Générateur d'en-tête TEI : formulaire spécialisé** (ID 162445), sera mis à disposition dès que possible.

Pour accéder à la liste complète des questionnaires importés, reportez-vous à la barre de navigation en haut de page et cliquez sur **Questionnaires**.  

Pour tout renseignement supplémentaire sur les modalités d’activation des questionnaires, reportez-vous à la page dédiée dans la documentation LimeSurvey : https://manual.limesurvey.org/Activating_a_survey/fr/

### Vérifications importantes après import du questionnaire  

- paramètres généraux > format  > tout en un
  - cliquer sur Sauvegarder
- paramètres de participation 
  - Permettre les réponses multiples ou les mises à jour de réponses avec une invitation :  OUI 
  - Permettre l’enregistrement public : OUI ou NON
  - cliquer sur Sauvegarder
- publication et accès : 
  - Rendre ce questionnaire public : OUI ou NON
  - cliquer sur Sauvegarder

***

<a name="3"/>

## 3. Import du thème

### Importer le thème du générateur d'en-tête TEI  

<u>Le thème associé au générateur d'en-tête TEI ne détermine pas seulement son identité graphique, mais aussi et surtout son comportement : il est donc très important que ce thème soit installé et associé à chaque questionnaire.</u>

1. Reportez-vous à la barre de navigation en haut de la page et cliquez sur  **Configuration**.  
2. Cliquez sur **Thèmes**  
3. Dans la section **Thèmes de questionnaire installés**, cliquez sur le bouton **Importer**.  
4. Une fenêtre s'ouvre. Pour importer le nouveau thème sous forme de fichier ZIP, cliquez sur **Browse**.  
5. Ouvrez le dossier **procedure_configuration_formulaire**.  
6. Cliquez sur le dossier compressé **generateur-entete-tei_theme.zip**.  
7. Le fichier ZIP est chargé. Pour finaliser l'import du thème, cliquez sur **Importer**.  

### Vérifications importantes après import du thème

Nous allons vérifier que chaque questionnaire est bien associé au thème **generateur-entete-tei_theme**.

1. Reportez-vous à la barre de navigation en haut de la page et cliquez sur  **Questionnaires**. 
2. Cliquez sur le questionnaire **Générateur d'en-tête TEI** (ID  534613).
3. Dans le menu latéral de gauche, cliquez sur **Paramètres généraux**.
4. Dans le bloc **Paramètres généraux du questionnaire**, reportez-vous à l'option **Modèle**, située en bas à droite.
5. Ouvrez le menu déroulant et sélectionnez le thème **generateur-entete-tei_theme**.
6. Cliquez sur **Sauvegarder** en haut à droite.

Retournez à la liste de questionnaires en cliquant sur **Questionnaires** en haut de la page.

Effectuez cette liste d'opérations pour tous les questionnaires qui composent le générateur d'en-tête TEI.

***

<a name="4"/>  

## 4. Activation du questionnaire

Une fois que les questionnaires et le thèmes sont importés et configurés, nous pouvons procéder à la publication du générateur d'en-tête TEI.

Pour ce faire, nous allons *activer* les quatre questionnaires qui le composent.

1. Reportez-vous à la barre de navigation en haut de la page et cliquez sur  **Questionnaires**. 
2. Cliquez sur le questionnaire **Générateur d'en-tête TEI** (ID  534613).
3. Reportez-vous aux options situées au-dessus de l'encadré **Résumé du questionnaire**.
4. Cliquez sur le bouton vert **Activer ce questionnaire**.
5. Un message d'avertissement s'ouvre. Réglez les paramètres en bas de page à votre souhait.
6. Cliquez sur **Sauvegarder et activer ce questionnaire**.
7. Vous pouvez choisir de basculer en mode **accès restreint**. 

   *En mode accès restreint, la participation au questionnaire est possible uniquement sur invitation. En mode «accès public», au contraire, aucune invitation n’est nécessaire pour répondre au questionnaire. Pour plus de renseignements sur les permissions de participation, reportez-vous à la page dédiée dans la documentation LimeSurvey : https://manual.limesurvey.org/Survey_participants*.

Retournez à la liste de questionnaires en cliquant sur **Questionnaires** en haut de la page. 

Effectuez cette liste d'opérations pour tous les questionnaires qui composent le générateur d'en-tête TEI.

### Vérification du statut d'un questionnaire

Pour vérifier le statut d'un questionnaire, cliquez sur **Questionnaires** en haut de la page.

Les renseignements sur les différents questionnaires sont organisés dans un tableau.

La deuxième colonne du tableau affiche le **Statut** de chaque questionnaire :
*  un carré rouge signale les questionnaires inactifs ; 
*  une flèche verte signale les questionnaires actifs.


Il est également possible de pre-visualiser un questionnaire actif pour vérifier si tous les paramètres ont bien été pris en compte.

1. Reportez-vous à la barre de navigation en haut de la page et cliquez sur  **Questionnaires**. 
2. Cliquez sur le questionnaire **Générateur d'en-tête TEI** (ID  534613).
3. Reportez-vous aux options situées au-dessus de l'encadré **Résumé du questionnaire** (centre de la page).
4. Cliquez sur le bouton **Lancer le questionnaire**.

La version publique du questionnaire s'ouvre dans une nouvelle page. Vous pourrez ainsi vérifier sa présentation et tester ses fonctionnalités.

***

<a name="5"/>

## 5. Génération de l'en-tête TEI

Votre questionnaire générateur d'en-tête TEI est désormais opérationnel. Comment faire pour générer un en-tête TEI en partant des réponses données ?

Ce procédé est exposé dans l'autre grand dossier de ce projet : [procedure_extraction_entete](https://gitlab.com/m.d.cristache/generateur-entete-tei/tree/master/procedure_extraction_entete)

***

<a name="6"/>

## 6. Fonctionnalités diverses

### Personnaliser le comportement et l’apparence du thème  

Le comportement et l’apparence du thème peuvent être personnalisés. Pour ce faire, reportez-vous à la page dédiée dans la documentation LimeSurvey : https://manual.limesurvey.org/Theme_editor.


### Personnaliser le logo du questionnaire

Le questionnaire peut présenter un logo situé en haut à gauche. Pour personnaliser le logo de votre questionnaire, reportez-vous à la page dédiée dans la documentation LimeSurvey : https://manual.limesurvey.org/Theme_options.

